using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _020920
{
    class Program
    {


        class Person : object, IComparable<Person>, IEquatable<Person>
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }

            public int CompareTo(Person other)
            {
                return this.Name.CompareTo(other.Name);
            }

            public override int GetHashCode()
            {
                return ID;
            }

            public override bool Equals(object obj)
            {
                Person obj_as_person = obj as Person;
                return this.ID == obj_as_person.ID;
            }

            // ==
            // !=

            public override string ToString()
            {
                return $"{base.ToString()} ID: {ID} Name : {Name} AGE:{Age}";
            }

            public bool Equals(Person other)
            {
                return this.ID == other.ID;
            }
        }
        class Car
        {
            public string Name { get; set; }
            public int Year { get; set; }
            public override string ToString()
            {
                return $"{base.ToString()} Name: {Name} Year: { Year}";
            }
        }


        static void Main(string[] args)
        {
            // throw new Exception(); // this will crash

            Dictionary<int, string> map_id_to_name = new Dictionary<int, string>();
            map_id_to_name.Add(1, "itay");
            //map_id_to_name.Add(1, "itay"); //unsafe

            string name;
            //Console.WriteLine(name); // error -- cannot used unassigned value without out
            bool result = map_id_to_name.TryGetValue(1, out name);
            result = map_id_to_name.TryGetValue(1, out string new_name);
            if (map_id_to_name.TryGetValue(1, out string new_name2))
            {
                Console.WriteLine(new_name2);
            }
            else
            {
                map_id_to_name.Add(1, "itay"); // safe . key = 1, value = "itay"
            }

            if (!map_id_to_name.ContainsKey(1))
            {
                map_id_to_name.Add(1, "danny"); // safe . key = 1, value = "itay"
            }
            map_id_to_name[1] = "danny"; // safe -- if exist then overwrite else create

            if (!map_id_to_name.ContainsValue("danny"))
            {
                map_id_to_name[1] = "danny"; // safe -- if exist then overwrite else create
            }

            KeyValuePair<int, int> my_value_pair = new KeyValuePair<int, int>(1, 3);
            Console.WriteLine(my_value_pair.Key); //1
            Console.WriteLine(my_value_pair.Value); //3

            int x93 = 93;
            int[] x94 = new int[2] { 1, 2 };
            KeyValuePair<int, string> pair_id_name = new KeyValuePair<int, string>(key: 1, value: "dorit");
            Console.WriteLine(pair_id_name.Key); // 1
            Console.WriteLine(pair_id_name.Value); // "dorit"

            Tuple<int, string> tuple = new Tuple<int, string>(1, "light");
            Console.WriteLine(tuple.Item1); // 1
            Console.WriteLine(tuple.Item2); // "light"

            // Dictionary<int, string> map_id_to_name = new Dictionary<int, string>();
            foreach (KeyValuePair<int, string> item in map_id_to_name)
            {
                Console.WriteLine($"KEY : {item.Key} VALUE : {item.Value}");
                
            }

            foreach (int one_key in map_id_to_name.Keys)
            {
                Console.WriteLine($"KEY : {one_key} VALUE : {map_id_to_name[one_key]}");
            }

            Console.WriteLine(name);

            Console.WriteLine("===============================");
            List<Person> people = new List<Person>()
            {
                new Person { ID = 1, Name = "Danny", Age = 22},
                new Person { ID = 2, Name = "Nemo", Age = 1},
                new Person { ID = 3, Name = "Moshe", Age = 45}
            };


            Dictionary<string, List<Person>> map_className_to_listOfStundets = new Dictionary<string, List<Person>>();
            List<Person> gimel8 = new List<Person>()
            {
                new Person { ID = 1, Name = "Danny", Age = 22},
                new Person { ID = 2, Name = "Nemo", Age = 1},
                new Person { ID = 3, Name = "Moshe", Age = 45}
            };
            map_className_to_listOfStundets.Add(key: "gimel8", value: gimel8);
            Person student_2_in_gimal8 = map_className_to_listOfStundets["gimel8"][2];
            List<Person> gimel8_from_dict = map_className_to_listOfStundets["gimel8"];
            gimel8_from_dict.Sort();    




            // i want to retrieve a person record by his id

            Dictionary<int, Person> map_id_to_person = new Dictionary<int, Person>();

            // populate the dictionary from the list
            foreach (Person p in people)
            {
                map_id_to_person.Add(key: p.ID, value: p);
            }
            // LINQ
            //people.ForEach(p =>
            //{
            //    map_id_to_person.Add(p.ID, p);
            //    map_id_to_person[p.ID] = p;
            //});
            //people.ForEach(p => map_id_to_person[p.ID] = p);

            //Dictionary<int, Person> map_id_to_person = new Dictionary<int, Person>()
            //{
            //    { 1, new Person { ID = 1, Name = "Danny", Age = 22} }
            //};

            // id == 3
            if (map_id_to_person.ContainsKey(3))
            {
                Person person_id_3 = map_id_to_person[3];
                Console.WriteLine($"id:3 result = {person_id_3}");
            }
            else
            {
                Console.WriteLine("person with id 3 not found");
            }

            if (map_id_to_person.TryGetValue(3, out Person p_id3))
            {
                Console.WriteLine($"id:3 result = {p_id3}");
            }
            else
            {
                Console.WriteLine("person with id 3 not found");
            }

            Person timi = new Person { ID = 100, Name = "Timi", Age = 30 };
            Car honda = new Car { Name = "Honda Civic", Year = 2020 };
            Dictionary<Person, Car> map_person_car = new Dictionary<Person, Car>();
            map_person_car.Add(key: timi, value: honda); // key:100 value:honda

            Car the_honda = map_person_car[timi];
            Console.WriteLine(the_honda);

            Car the_honda_using_hash_code = map_person_car[new Person { ID = 100, Name = "Timi", Age = 30 }];

            if (timi.Equals(new Person { ID = 100, Name = "Timi", Age = 30 }))
            {
                Console.WriteLine();
            }
        }
    }
}
